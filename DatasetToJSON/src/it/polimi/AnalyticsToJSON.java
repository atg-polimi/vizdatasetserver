package it.polimi;

import it.polimi.analytics.NodeAnalytics;
import it.polimi.analytics.TransitionAnalytics;
import it.polimi.entity.Node;
import it.polimi.entity.Transition;
import it.polimi.utils.Dataset;
import it.polimi.utils.DatasetReader;
import it.polimi.utils.Label;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AnalyticsToJSON extends HttpServlet {

	private static final long serialVersionUID = 1142785905931939611L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		// Read dataset file
//		String dataset = new DatasetReader(this.getServletContext().getRealPath(Dataset.DATASET_1), Label.ROOM_4).readFile();
		
		//parameters
		String dayPar = req.getParameter("d");
		String startIdpar = req.getParameter("startId");
		String endIdpar = req.getParameter("endId");
		int startId = 1;
		int endId = 287;
		int dayId = 1;
		
		if (startIdpar == null) startId = 1; else startId = Integer.parseInt(startIdpar); 
		if (endIdpar == null) endId = 287; else endId = Integer.parseInt(endIdpar);
		if (dayPar == null) dayId = 1; else dayId = Integer.parseInt(dayPar);
		
		// Get analytics
//		JsonObject json = getAnalyticsJSON(dataset.toString(),startId,endId);
		JsonObject json = getAnalyticsJSON(dayId,startId,endId);
		
		// Send Response
		res.setHeader("Access-Control-Allow-Origin", "*");		// Needed by jQuery.ajax()
		res.setContentType("application/json");
		
		PrintWriter out = res.getWriter();
		out.print(json);
		out.flush();
	}
	
	// Retrieve analytics in JSON format
	private JsonObject getAnalyticsJSON(int dayId, int startId, int endId) {		
		JsonObject outcome = new JsonObject();

		String path = Dataset.DATASET_2_2 + "day" + (132+dayId) + ".csv";
		String dataset = new DatasetReader(this.getServletContext().getRealPath(path), Label.ROOM_4).readFile();
		
		List<Node> nodes = new NodeAnalytics(Label.ROOM_4).getAnalytics(dataset,startId,endId);
		outcome.add("nodes", new Gson().toJsonTree(nodes));

		List<Transition> transitions = new TransitionAnalytics().getAnalytics(dataset,startId,endId);
		outcome.add("transitions", new Gson().toJsonTree(transitions));
		
		return outcome;
	}
	
}
