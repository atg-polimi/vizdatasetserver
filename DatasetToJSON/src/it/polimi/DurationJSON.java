package it.polimi;

import it.polimi.utils.Dataset;
import it.polimi.utils.DatasetReader;
import it.polimi.utils.Label;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class DurationJSON extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3840450495765185467L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		// Read dataset file
		String dayPar = req.getParameter("d");
		int dayId = 1;
		if (dayPar == null) dayId = 1; else dayId = Integer.parseInt(dayPar);
		
		// Create JSON file
		JsonArray json = datasetToJSON(dayId);
		
		// Send Response
		res.setHeader("Access-Control-Allow-Origin", "*");		// Needed by jQuery.ajax()
		res.setContentType("application/json");
		
		PrintWriter out = res.getWriter();
		out.print(json);
		out.flush();
	}
	
	
	private JsonArray datasetToJSON(int dayId) {
		String path = Dataset.DATASET_2_2 + "day" + (132+dayId) + ".csv";
		String dataset = new DatasetReader(this.getServletContext().getRealPath(path), Label.ROOM_4).readFile();
		
		JsonArray records = new JsonArray();
		char currentPlace = dataset.charAt(0);
		int duration = 1;
		int startId = 1;
		for (int i = 0; i < dataset.length(); i++) {
			if (currentPlace == dataset.charAt(i)) {
				duration++;
			} else {
				JsonObject r = new JsonObject();
				r.add("value", new Gson().toJsonTree(String.valueOf(currentPlace)));
				r.add("startId", new Gson().toJsonTree(startId + ""));
				r.add("endId", new Gson().toJsonTree((i-1) + ""));
				//r.add("duration", new Gson().toJsonTree(duration + ""));
				records.add(r);
				
				currentPlace = dataset.charAt(i);
				duration = 1;
				startId = i;
			}
		}
		//the last one
		JsonObject r = new JsonObject();
		r.add("value", new Gson().toJsonTree(String.valueOf(currentPlace)));
		r.add("startId", new Gson().toJsonTree(startId + ""));
		r.add("endId", new Gson().toJsonTree(dataset.length() + ""));
		//r.add("duration", new Gson().toJsonTree(duration + ""));
		records.add(r);

		
		return records;
	}

}
