package it.polimi.log;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Query.SortDirection;

public class LogQuery extends HttpServlet {

	private static final long serialVersionUID = 4473212053537261850L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	    String name = req.getParameter("name");

	    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	    Filter fileName = new FilterPredicate("fileName", FilterOperator.EQUAL, name);
	    
	    Query q = new Query("Log")
	    	.setFilter(fileName)
	    	.addSort("order", SortDirection.ASCENDING);
	    
	    PreparedQuery pq = datastore.prepare(q);
	    
	    String txt = "";
	    for (Entity result : pq.asIterable()) {
	    	Text content = (Text) result.getProperty("content");
	    	txt += content.getValue();
    	}
	    
	    resp.setHeader("Content-Disposition", "attachment; filename=" + name + ".txt");	    
	    
	    PrintWriter out = resp.getWriter();
		out.print(txt);
		out.flush();
		out.close();
	}

}