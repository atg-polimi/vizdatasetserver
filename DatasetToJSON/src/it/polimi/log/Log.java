package it.polimi.log;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;

public class Log extends HttpServlet {

	private static final long serialVersionUID = 4473212053537261850L;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	    String fileName = req.getParameter("name");
	    int index = Integer.parseInt(req.getParameter("order"));
	    Key logKey = KeyFactory.createKey("Log", fileName);
	    Text content = new Text(req.getParameter("content"));
	   
	    Entity log = new Entity("Log", logKey);
	    log.setProperty("fileName", fileName);
	    log.setProperty("content", content);
	    log.setProperty("order", index);

	    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	    datastore.put(log);
	    
	    resp.setHeader("Access-Control-Allow-Origin", "*");		// Needed by jQuery.ajax()
	    
	}

}
