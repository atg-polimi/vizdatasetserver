package it.polimi.entity;

public class Transition {
	
	private String from;
	private String to;
	private int count;
	private double percentage;
	
	public Transition() {
		this.from = "";
		this.to = "";
		this.count = 0;
		this.percentage = 0;
	}
	
	public Transition(String from, String to, int count, double percentage) {
		this.from = from;
		this.to = to;
		this.count = count;
		this.percentage = percentage;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
	
}
