package it.polimi.entity;

public class Node {
	
	private String type;
	private int count;
	private double percentage;
		
	public Node() {
		this.type = "";
		this.count = 0;
		this.percentage = 0;
	}
	
	public Node(String type, int count, float percentage) {
		this.type = type;
		this.count = count;
		this.percentage = percentage;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
	
}
