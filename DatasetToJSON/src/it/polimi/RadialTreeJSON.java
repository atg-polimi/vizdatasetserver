package it.polimi;

//import it.polimi.analytics.NodeAnalytics;
//import it.polimi.entity.Node;
//import it.polimi.utils.Dataset;
//import it.polimi.utils.DatasetReader;
//import it.polimi.utils.Label;

import java.io.IOException;
import java.io.PrintWriter;
//import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.joda.time.DateTime;
//import org.joda.time.format.DateTimeFormat;
//import org.joda.time.format.DateTimeFormatter;

//import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


@Deprecated
public class RadialTreeJSON extends HttpServlet {	

	private static final long serialVersionUID = -5305364554906880021L;
	
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		JsonObject root = getJSONStructure();
			
		// Send Response
		res.setHeader("Access-Control-Allow-Origin", "*");		// Needed by jQuery.ajax()
		res.setContentType("application/json");
		
		PrintWriter out = res.getWriter();
		out.print(root);
		out.flush();
	}
	
	
	// Create year-season-month JSON structure
	private JsonObject getJSONStructure() {
//		DateTime dt = new DateTime(2014, 1, 1, 0, 0);
//		
//		JsonObject root = new JsonObject();
//		root.addProperty("name", dt.year().get());
//	
//		JsonArray seasons = new JsonArray();
//		for (int i = 0; i < Label.SEASONS.length; i++) {
//			JsonObject season = new JsonObject();
//			season.addProperty("name", Label.SEASONS_NAME[i]);
//
//			JsonArray months = new JsonArray();
//			for (int j = 0; j < Label.SEASONS[i].length; j++) {
//				JsonObject month = new JsonObject();
//				month.addProperty("name", Label.SEASONS[i][j]);
//
//				JsonArray days = new JsonArray();
//				for (int k = 1; k <= dt.dayOfMonth().get(); k++) {					
//					JsonObject day = new JsonObject();
//					day.addProperty("name", dt.dayOfYear().get());
//					
//					DateTimeFormatter fmt = DateTimeFormat.forPattern("yMMdd");
//					day.addProperty("date", fmt.print(dt));//dt.getDayOfMonth() + "/" + dt.getMonthOfYear() + "/" + dt.getYear());
//					
//					List<Node> rooms = getDayAnalytics(dt.dayOfYear().get());
//					for (int r = 0; r < Label.ROOM_4.length; r++) {
//						day.addProperty(Label.ROOM_4[r], rooms.get(r).getPercentage());
//					}
//					days.add(day);
//					dt = dt.plusDays(1);
//				}
//				month.add("days", days);
//				months.add(month);
//			}
//			season.add("months", months);
//			seasons.add(season);
//		}
//		root.add("seasons", seasons);
//		
//		return root;
		return null;
	}
	

//	private List<Node> getDayAnalytics(int dayOfYear) {
//		//String path = Dataset.DATASET_2_1 + "day" + dayOfYear + ".csv";
//		String path = Dataset.DATASET_2_2 + "day" + (132+dayOfYear) + ".csv";
//		String dataset = new DatasetReader(this.getServletContext().getRealPath(path), Label.ROOM_4).readFile();
//			
//		return new NodeAnalytics(Label.ROOM_4).getAnalytics(dataset,1,287);
//	}
		
}
