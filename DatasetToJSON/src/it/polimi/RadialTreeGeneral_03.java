package it.polimi;

import it.polimi.analytics.NodeAnalytics;
import it.polimi.entity.Node;
import it.polimi.utils.Dataset;
import it.polimi.utils.DatasetReader;
import it.polimi.utils.Label;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class RadialTreeGeneral_03 extends HttpServlet {

	private static final long serialVersionUID = -6694728718955476309L;
	
	private static final String children = "children";
	private static final String[] rooms = Label.ROOM_4;
	private static final DateTimeFormatter output = DateTimeFormat.forPattern("dd/MM/y");
	
	private LocalDate sdate;
	private LocalDate edate;
	private int counter;
		
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		sdate = new LocalDate(2013, 10, 25);
		edate = new LocalDate(2014, 12, 31);
		counter = 0;
		JsonObject root = getJSONStructure();
			
		// Send Response
		res.setHeader("Access-Control-Allow-Origin", "*");		// Needed by jQuery.ajax()
		res.setContentType("application/json");
		
		PrintWriter out = res.getWriter();
		out.print(root);
		out.flush();
	}
	
	
	// Create year-season-month-day JSON structure
	private JsonObject getJSONStructure() {
		JsonObject root = new JsonObject();
		root.addProperty("name", "period");
		root.addProperty("id", "period");
		
		JsonArray years = new JsonArray();
		int diff = edate.getYear() - sdate.getYear();
		for(int i = 0; i <= diff; i++) {
			JsonObject year = new JsonObject();
			year.addProperty("name", sdate.getYear());
			year.addProperty("id", sdate.getYear());
			
			JsonArray seasons = new JsonArray();
			for (int s = Label.MONTHSEASON[sdate.getMonthOfYear()-1]; s < Label.SEASON.length; s++) {
				String yearLabel = String.valueOf(((sdate.getMonthOfYear() == 12) ? sdate.getYear()+1 : sdate.getYear())).substring(2);
				
				JsonObject season = new JsonObject();
				season.addProperty("name", Label.SEASON[s]);
				season.addProperty("id", Label.SEASON[s] + yearLabel);
	
				JsonArray months = new JsonArray();
//				for (int m = 0; m < Label.SEASONS_LENGTH[s]; m++) {
				int prev = Label.MONTHSEASON[sdate.getMonthOfYear()-1];
				LocalDate temp = sdate;
				while (prev == Label.MONTHSEASON[temp.getMonthOfYear()-1]) {
					JsonObject month = new JsonObject();
					month.addProperty("name", Label.MONTHS[sdate.getMonthOfYear()-1]);
					month.addProperty("id", Label.MONTHS[sdate.getMonthOfYear()-1].substring(0,3) + yearLabel);
	
					JsonArray days = new JsonArray();
//					for (int d = 1; d <= sdate.dayOfMonth().get(); d++) {
					int d = sdate.getDayOfMonth();
					while ((d <= (sdate.monthOfYear().withMaximumValue()).getDayOfMonth()) && !(sdate.isAfter(edate))) {
						JsonObject day = new JsonObject();
						day.addProperty("name", output.print(sdate));
						day.addProperty("id", counter++);
						
						List<Node> room = getDayAnalytics(counter);
						for (int r = 1; r <= rooms.length; r++) 
							day.addProperty("value"+String.format("%02d", r), room.get(r-1).getPercentage());
						
						days.add(day);
						sdate = sdate.plusDays(1);
						d++;
					}
					prev = Label.MONTHSEASON[sdate.getMonthOfYear()-1];					
					month.add(children, days);
					months.add(month);
				}
				season.add(children, months);
				seasons.add(season);
			}
			year.add(children, seasons);
			years.add(year);
		}
		root.add(children, years);

		return root;
	}
	
	private List<Node> getDayAnalytics(int dayOfYear) {
		String path = Dataset.DATASET_2_2 + "day" + (dayOfYear) + ".csv";
		String dataset = new DatasetReader(this.getServletContext().getRealPath(path), rooms).readFile();
			
		return new NodeAnalytics(rooms).getAnalytics(dataset,1,287);
	}

}
