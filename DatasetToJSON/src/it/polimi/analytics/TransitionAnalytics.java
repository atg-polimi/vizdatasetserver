package it.polimi.analytics;

import it.polimi.entity.Transition;
import it.polimi.utils.Label;

import java.util.ArrayList;
import java.util.List;

public class TransitionAnalytics extends Analytics {

	private List<Transition> transitions;
    	
    public TransitionAnalytics() {
    	this.count = this.initMap(Label.TRANSITION_4);
    	this.transitions = new ArrayList<Transition>();
	}
    	
	@Override
	public List<Transition> getAnalytics(String dataset,int startId, int endId) {
		if (dataset.length() > 0) {
			
			// Compute transitions' count
			int numTransition = 0;
			String previous = "";
			for (int i = 0; i < dataset.length(); i++) {
				String type = String.valueOf(dataset.charAt(i));
				String transition = previous + type;
	
				try {
					this.count.put(transition, this.count.get(transition) + 1);
					numTransition ++;
				} catch (NullPointerException e) { }
				
				previous = type;
			}
			
			// Compute transitions' percentages
			for (String transition : this.count.keySet()) {
				Transition t = new Transition();
				t.setFrom(String.valueOf(transition.charAt(0)));
				t.setTo(String.valueOf(transition.charAt(1)));
				t.setCount(this.count.get(transition));
				t.setPercentage((double)this.count.get(transition) / numTransition);
				
				transitions.add(t);
			}
		}
		
		return transitions;
	}
	
}


