package it.polimi.analytics;

import it.polimi.entity.Node;
import it.polimi.utils.NodeSorter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NodeAnalytics extends Analytics {
	
	private List<Node> nodes;

	public NodeAnalytics(String[] nodes) {
		this.count = this.initMap(nodes);
		this.nodes = new ArrayList<Node>();
	}
	
	@Override
	public List<Node> getAnalytics(String dataset, int startId, int endId) {
		if (dataset.length() > 0) {
//			 Compute nodes' count
//			for (int i = 0; i < dataset.length(); i++) {
			for (int i = startId; i < endId; i++) {
				String type = String.valueOf(dataset.charAt(i));
				
				if (Arrays.asList(this.count.keySet().toArray()).contains(type))
					this.count.put(type, this.count.get(type)+1);
			}
			
			// Set outcome
			for (String node : this.count.keySet()) {
				Node n = new Node();
				n.setType(node);
				n.setCount(this.count.get(node));
				//n.setPercentage((float)(this.count.get(node)) / dataset.length());
				n.setPercentage((float)(this.count.get(node)) / (endId-startId+1));
				
				nodes.add(n);
			}
			
			Collections.sort(nodes, new NodeSorter());
		}
		
		return nodes;
	}
	
}
