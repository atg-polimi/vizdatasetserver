package it.polimi.analytics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Analytics {
		
	protected Map<String, Integer> count;
		
	protected HashMap<String, Integer> initMap(String[] keys) {
		HashMap<String, Integer> result = new HashMap<String, Integer>();
		
		for (int i = 0; i < keys.length; i++)
			result.put(keys[i], 0);
		
		return result;
	}
	
	public abstract List<? extends Object> getAnalytics(String dataset, int startId, int endId);
	
}
