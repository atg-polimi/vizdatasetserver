package it.polimi;

import it.polimi.utils.Dataset;
import it.polimi.utils.DatasetReader;
import it.polimi.utils.Label;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class DatasetToJSON extends HttpServlet {

	private static final long serialVersionUID = 8174431269252402025L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		// Read dataset file
//		String dataset = new DatasetReader(this.getServletContext().getRealPath(Dataset.DATASET_1), Label.ROOM_4).readFile();
		String dayPar = req.getParameter("d");
		int dayId = 1;
		if (dayPar == null) dayId = 1; else dayId = Integer.parseInt(dayPar);
		
		// Create JSON file
		JsonArray json = datasetToJSON(dayId);
		
		// Send Response
		res.setHeader("Access-Control-Allow-Origin", "*");		// Needed by jQuery.ajax()
		res.setContentType("application/json");
		
		PrintWriter out = res.getWriter();
		out.print(json);
		out.flush();
	}
	
	
	private JsonArray datasetToJSON(int dayId) {
		String path = Dataset.DATASET_2_2 + "day" + (132+dayId) + ".csv";
		String dataset = new DatasetReader(this.getServletContext().getRealPath(path), Label.ROOM_4).readFile();
		
		JsonArray records = new JsonArray();
		for (int i = 0; i < dataset.length()-1; i++) {
			JsonObject r = new JsonObject();
			r.add("id", new Gson().toJsonTree(i+1));
			r.add("value", new Gson().toJsonTree(String.valueOf(dataset.charAt(i))));
			
			records.add(r);
		}
		
		return records;
	}

}
