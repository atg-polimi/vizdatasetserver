package it.polimi;

import it.polimi.analytics.NodeAnalytics;
import it.polimi.entity.Node;
import it.polimi.utils.Dataset;
import it.polimi.utils.DatasetReader;
import it.polimi.utils.Label;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class RadialTreeGeneral_01 extends HttpServlet {

	private static final long serialVersionUID = -6694728718955476309L;
	
	private static final String children = "children";
	private static final String[] rooms = Label.ROOM_3;
	private static final DateTimeFormatter output = DateTimeFormat.forPattern("dd/MM/y");
	
	private LocalDate date;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		date = new LocalDate(2014, 1, 1);
		
		JsonObject root = getJSONStructure();
			
		// Send Response
		res.setHeader("Access-Control-Allow-Origin", "*");		// Needed by jQuery.ajax()
		res.setContentType("application/json");
		
		PrintWriter out = res.getWriter();
		out.print(root);
		out.flush();
	}
	
	
	// Create year-season-month JSON structure
	private JsonObject getJSONStructure() {
		JsonObject root = new JsonObject();
		root.addProperty("name", date.getYear());
		root.addProperty("id", "year" + date.getYear());
	
		JsonArray seasons = new JsonArray();
		for (int s = 0; s < Label.SEASON.length; s++) {
			String yearLabel = String.valueOf(((date.getMonthOfYear() == 12) ? date.getYear()+1 : date.getYear())).substring(2);
			
			JsonObject season = new JsonObject();
			season.addProperty("name", Label.SEASON[s]);
			season.addProperty("id", Label.SEASON[s] + yearLabel);

			JsonArray months = new JsonArray();
			for (int m = 0; m < Label.SEASONS_LENGTH[s]; m++) {
				JsonObject month = new JsonObject();
				month.addProperty("name", Label.MONTHS[date.getMonthOfYear()-1]);
				month.addProperty("id", Label.MONTHS[date.getMonthOfYear()-1].substring(0,3) + yearLabel);

				JsonArray days = new JsonArray();
				for (int d = 1; d <= date.dayOfMonth().get(); d++) {
					JsonObject day = new JsonObject();
					day.addProperty("name", output.print(date));
					day.addProperty("id", date.getDayOfYear());
					
					List<Node> room = getDayAnalytics(date.getDayOfYear());
					for (int r = 1; r <= rooms.length; r++) 
						day.addProperty("value"+String.format("%02d", r), room.get(r-1).getPercentage());
					
					days.add(day);
					date = date.plusDays(1);
				}
				month.add(children, days);
				months.add(month);
			}
			season.add(children, months);
			seasons.add(season);
		}
		root.add(children, seasons);
		
		return root;
	}
	

	private List<Node> getDayAnalytics(int dayOfYear) {
		String path = Dataset.DATASET_2_2 + "day" + (132+dayOfYear) + ".csv";
		String dataset = new DatasetReader(this.getServletContext().getRealPath(path), rooms).readFile();
			
		return new NodeAnalytics(rooms).getAnalytics(dataset,1,287);
	}

}
