package it.polimi.utils;

import it.polimi.entity.Node;
import java.util.Comparator;

public class NodeSorter implements Comparator<Node> {
	@Override
	public int compare(Node n1, Node n2) {
		if (n1.getType().charAt(0) < n2.getType().charAt(0))
			return -1;
		else if (n1.getType().charAt(0) == n2.getType().charAt(0))
			return 0;
		else 
			return 1;
	}
}
