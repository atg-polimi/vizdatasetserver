package it.polimi.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

public class DatasetReader {

	private String filePath;
	private String[] rooms;
	
	public DatasetReader(String path, String[] rooms) {
		this.filePath = path;
		this.rooms = rooms;
	}
	
	public String readFile() {
		InputStream in;
		StringBuilder dataset = new StringBuilder();
		try {
			in = new FileInputStream(new File(filePath));
			BufferedReader reader;
			reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			
			String previous = "B";
			String line;
			while ((line = reader.readLine()) != null) {
				String record = line;
				try {
					record = String.valueOf( (char) Integer.parseInt(line) );
				} catch (NumberFormatException e) { /* No Problem*/ }
				if (! Arrays.asList(this.rooms).contains(record)) 
					dataset.append(previous); 
				else {
					dataset.append(record);
					previous = record;
				}				
			}
			reader.close();
			in.close();
		} catch (IOException e) { 
			dataset.append("");
			e.printStackTrace(); }
		
		return dataset.toString();
	}
}
