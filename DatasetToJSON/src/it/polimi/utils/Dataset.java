package it.polimi.utils;

public final class Dataset {
	
	public static final String DATASET_1 = "data/oldperson.dat";
	
	public static final String DATASET_2_1 = "data/dataset-1/";

	public static final String DATASET_2_2 = "data/dataset-2/";
	
	public static final String DATASET_2_3 = "data/dataset-3/";
	
}
