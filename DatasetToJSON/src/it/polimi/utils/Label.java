package it.polimi.utils;

public final class Label {
	
	public static final String[] ROOM_4 = {"B", "K", "L", "T"};
	public static final String[] ROOM_3 = {"B", "K", "L"};
	
	public static final String[] TRANSITION_4 = {"BK", "BL", "BT", "KB", "KL", "KT", "LB", "LK", "LT", "TB", "TK", "TL"};
	public static final String[] TRANSITION_3 = {"BK", "BL", "KB", "KL", "LB", "LK"};
	
	public static final String[] SEASON 		= {"winter", "spring", "summer", "fall", "winter"};
	public static final int[] SEASONS_LENGTH	= {2, 3, 3, 3, 1};
	
	public static final String[] MONTHS = {"january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};

	public static final int[] MONTHSEASON = {0,0,1,1,1,2,2,2,3,3,3,4};
}